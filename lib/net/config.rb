# frozen_string_literal: true

require "net/config/version"
require "active_support/core_ext/hash/indifferent_access"
require "active_support/core_ext/hash/deep_merge"
require "active_support/core_ext/object/deep_dup"
require "active_support/core_ext/hash"
require "yaml"
require "json"
require "erb"
require "toml-rb"

module Net
  module ConfigModule
    class Error < StandardError; end

    def initialize(config: {}, facts: {}, load_paths: nil)
      self.config = ActiveSupport::HashWithIndifferentAccess.new(**config)
      self.load_paths = load_paths || ENV["NET_CONFIG_LOAD_PATHS"]&.split(",") || [File.join(Dir.pwd, ".config", "net_config"), File.join(ENV["HOME"], ".config", "net_config")]
      self.facts = ActiveSupport::HashWithIndifferentAccess.new(**facts)
    end

    attr_accessor :config
    attr_accessor :load_paths, :config_files
    attr_accessor :facts, :facts_order

    def stringify_keys!
      self.config = config.deep_stringify_keys
    end
    alias deep_stringify_keys! stringify_keys!

    def symbolize_keys!
      self.config = config.deep_symbolize_keys
    end
    alias deep_symbolize_keys! symbolize_keys!

    def deep_merge!(hash)
      self.config = deep_merge(hash)
    end
    alias merge! deep_merge!

    def deep_merge(hash)
      config.deep_merge!(ActiveSupport::HashWithIndifferentAccess.new(hash))
    end
    alias merge deep_merge

    def set(*keys, value: nil)
      keys = keys.flatten
      last_key = keys.pop
      subset = full_config = ActiveSupport::HashWithIndifferentAccess.new
      keys.each do |key|
        subset[key] = ActiveSupport::HashWithIndifferentAccess.new unless subset[key].is_a?(Hash)
        subset = subset[key]
      end
      subset[last_key] = value
      deep_merge!(full_config)
      self
    end

    def []=(*keys, value)
      set(*keys, value: value)
    end

    def fetch(*keys, default: nil)
      keys = keys.flatten
      subset = config
      last_key = keys.pop
      keys.each do |key|
        unless subset.is_a?(Hash) && subset.key?(key)
          subset = nil
          return default
        end
        subset = subset[key]
      end
      subset.key?(last_key) ? subset[last_key] : default
    end
    alias [] fetch

    def fetch!(*keys, default: nil)
      keys = keys.flatten
      subset = config
      last_key = keys.pop
      keys.each do |key|
        unless subset.is_a?(Hash) && subset.key?(key)
          subset = nil
          raise Net::Node::Error, "#{keys.map(&:inspect).join(', ')} not found"
        end
        subset = subset[key]
      end
      subset.key?(last_key) ? subset[last_key] : raise(Net::Node::Error, "#{keys.map(&:inspect).join(', ')} not found")
    end

    def flat(*keys)
      flat_config = ActiveSupport::HashWithIndifferentAccess.new
      value = fetch(*keys)
      case value
      when Hash
        value.each do |key, _value|
          flat_config.merge! flat(*(keys + [key]))
        end
      else
        flat_config[keys] = value
      end
      flat_config
    end

    def config_files
      return @config_files if @config_files && (ENV["NET_CONFIG_ENV"] == "production")

      @config_files = load_paths.map do |load_path|
        Dir.glob(File.join(load_path, "*.*")) + Dir.glob(File.join(load_path, "*/*.*"))
      end.flatten.select { |file| File.file?(file) }
    end

    def facts_order
      @facts_order || facts&.keys
    end

    attr_accessor :lookup_proc
    def merge_by_facts(additional_facts = facts)
      (facts_order || additional_facts.keys).map do |key, value|
        deep_merge! lookup_proc&.call || yield(key, value)
      end.flatten
    end

    def merge_files_by_facts(additional_facts = facts)
      (facts_order || additional_facts.keys).map do |key, value|
        matching_files = config_files.grep(%r{#{key}/#{value}(/.*)?\.\w{3,4}(\.erb)?\z})
        merge_files matching_files
        matching_files
      end.flatten
    end

    def merge_files(*files)
      files = files.flatten
      files.each do |file|
        next unless File.file?(file)

        content = File.read(file)
        content = ERB.new(content).result(binding) if File.extname(file) == ".erb"
        case file[/\.\w{3,4}(\.erb)?\z/]
        when ".rb"
          deep_merge!(eval(content, binding, file))
        when ".yaml", ".yml", ".yaml.erb", ".yml.erb"
          deep_merge!(YAML.load(content))
        when ".json", ".json.erb"
          deep_merge!(JSON.parse(content))
        when ".toml", ".toml.erb"
          deep_merge!(TomlRB.parse(content))
        when ".xml", ".xml.erb"
          deep_merge!(Hash.from_xml(content))
        end
      end
      self
    end
    alias merge_file merge_files

    def to_rb
      Marshal.dump config
    end

    def to_json(*_args)
      config.to_json
    end

    def to_yaml
      config.to_yaml
    end

    def to_xml
      config.to_xml
    end

    def deep_dup
      config.deep_dup
    end

    def clone
      self.class.new(config: deep_dup)
    end
  end
end

module Net
  class Config
    include ConfigModule
  end
end
