# frozen_string_literal: true

RSpec.describe Net::Config do
  it "has a version number" do
    expect(Net::Config::VERSION).not_to be nil
  end

  let(:config) { Net::Config.new }

  it "has an instance" do
    expect(config).to be_a(Net::Config)
  end

  it "works deep" do
    config.set(:a, :b, :c, :d, value: :e)
    expect(config.fetch(:a, :b, :c, :d)).to eq(:e)
  end

  it "works deep" do
    config.set(:a, :b, :c, :d, value: :e)
    config.set(:a, :b, :c, :d, value: :f)
    expect(config.fetch(:a, :b, :c, :d)).to eq(:f)
  end

  it "non destructive" do
    config.set(:a, :b, :c, :d, value: :e)
    expect(config.fetch(:a, :b, :c, :d)).to eq(:e)
    expect(config.fetch(:a, :b, :c, :d)).to eq(:e)
  end

  it "overwrites" do
    config.set(:a, :b, value: :e)
    config.set(:a, :b, :c, value: :e)
    config.set(:a, :b, :c, :d, value: :e)
    expect(config.fetch(:a, :b, :c, :d)).to eq(:e)
  end

  it "returns a hash too" do
    config.set(:a, :b, :c, :d, value: :e)
    expect(config.fetch(:a, :b)).to eq("c" => { "d" => :e })
  end

  it "loves nil" do
    config.set(nil, nil, nil, nil, value: :e)
    expect(config.fetch(nil, nil, nil, nil)).to eq(:e)
    expect(config.fetch(nil, nil, nil)).to eq(nil => :e)
  end

  it "loves nil" do
    config.set(nil, nil, nil, nil, value: nil)
    expect(config.fetch(nil, nil, nil, nil)).to eq(nil)
    expect(config.fetch(nil, nil, nil)).to eq(nil => nil)
  end

  it "likes []" do
    config[:a, :b, :c, :d] = :f
    expect(config[:a, :b, :c, :d]).to eq(:f)
  end

  it "knows certain files" do
    config.load_paths = [File.join(__dir__, "examples")]
    expect(config.config_files.first).to match(/example\.\w+/)
  end

  it "loads everything parsable" do
    config.merge_files(*Dir.glob(File.join(__dir__, "examples/*")))
    expect(config.config).to eq("yaml" => true, "yaml_erb" => true, "json" => true, "json_erb" => true, "toml" => true, "toml_erb" => true)
  end

  it "ensures ActiveSupport::HashWithIndifferentAccess " do
    config.deep_merge!(a: { b: { c: {}, d: [] } })
    expect(config.fetch(:a)).to be_a(ActiveSupport::HashWithIndifferentAccess)
    expect(config.fetch(:a, :b)).to be_a(ActiveSupport::HashWithIndifferentAccess)
    expect(config.fetch(:a, :b, :c)).to be_a(ActiveSupport::HashWithIndifferentAccess)
    expect(config.fetch(:a)[:b]).to be_a(ActiveSupport::HashWithIndifferentAccess)
    expect(config.fetch(:a)["b"]["c"]).to be_a(ActiveSupport::HashWithIndifferentAccess)
  end

  it "it uses alternative requires too" do
    expect(require("net-config")).to be(true)
  end

  it "can be flat too" do
    config.deep_merge!(a: { b: { c: {}, d: [], e: true } })
    config.symbolize_keys!
    expect(config.flat(:a)).to eq(%i[a b d] => [], %i[a b e] => true)
  end
end
